package fr.danycorbineau.elog.manager.model;

import javax.persistence.Id;

import javax.persistence.*;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "amount_in_crate", nullable = false)
    private Integer amountInCrate;

    @Column(name = "image_url", nullable = false)
    private String imageUrl;

    @Column(name = "b_mat_cost", nullable = false)
    private Integer bMatCost;

    @Column(name = "r_mat_cost", nullable = false)
    private Integer rMatCost;

    @Column(name = "e_mat_cost", nullable = false)
    private Integer eMatCost;

    @Column(name = "h_mat_cost", nullable = false)
    private Integer hMatCost;

    public Item(String name, Integer amountInCrate, String imageUrl, Integer bMatCost, Integer rMatCost, Integer eMatCost, Integer hMatCost) {
        this.name = name;
        this.amountInCrate = amountInCrate;
        this.imageUrl = imageUrl;
        this.bMatCost = bMatCost;
        this.rMatCost = rMatCost;
        this.eMatCost = eMatCost;
        this.hMatCost = hMatCost;
    }

    public Item() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmountInCrate() {
        return amountInCrate;
    }

    public void setAmountInCrate(Integer amountInCrate) {
        this.amountInCrate = amountInCrate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getbMatCost() {
        return bMatCost;
    }

    public void setbMatCost(Integer bMatCost) {
        this.bMatCost = bMatCost;
    }

    public Integer getrMatCost() {
        return rMatCost;
    }

    public void setrMatCost(Integer rMatCost) {
        this.rMatCost = rMatCost;
    }

    public Integer geteMatCost() {
        return eMatCost;
    }

    public void seteMatCost(Integer eMatCost) {
        this.eMatCost = eMatCost;
    }

    public Integer gethMatCost() {
        return hMatCost;
    }

    public void sethMatCost(Integer hMatCost) {
        this.hMatCost = hMatCost;
    }

}
