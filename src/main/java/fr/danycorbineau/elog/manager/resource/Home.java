package fr.danycorbineau.elog.manager.resource;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("")
public class Home {

    @GET
    @Path("/help")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHome() {
        List<String> paths = new ArrayList<>();
        paths.add("/login");
        paths.add("/logout");
        paths.add("/register");
        return Response.status(200).entity(paths).build();
    }
}
