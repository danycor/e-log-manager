package fr.danycorbineau.elog.manager;

import fr.danycorbineau.elog.manager.resource.Home;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class ELogManagerApplication {
	@Bean
    ResourceConfig resourceConfig() {
		return new ResourceConfig().register(Home.class);
	}

    public static void main(String[] args) {
        SpringApplication.run(ELogManagerApplication.class, args);
    }
}
