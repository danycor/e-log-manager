package fr.danycorbineau.elog.manager;

import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomContainer implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    public void customize(ConfigurableServletWebServerFactory factory) {
        int port = 8081;
        if (System.getProperty("APP_PORT") != null) {
            port = Integer.parseInt(System.getProperty("APP_PORT"));
        }
        factory.setPort(port);
    }
}