package fr.danycorbineau.elog.manager.repository;

import fr.danycorbineau.elog.manager.model.Item;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemTests {
    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void setUp() {
        // Save 4 Person objects into the database
        itemRepository.save(new Item("first item", 20, "https://static.wikia.nocookie.net/foxhole_gamepedia_en/images/3/3d/Hydra%27s_Whisper_Icon.png/revision/latest/scale-to-width-down/30?cb=20210417155345", 100, 0, 10, 0));
        itemRepository.save(new Item("sec item", 10, "https://static.wikia.nocookie.net/foxhole_gamepedia_en/images/3/3d/Hydra%27s_Whisper_Icon.png/revision/latest/scale-to-width-down/30?cb=20210417155345", 0, 20, 0, 0));
        itemRepository.save(new Item("th item", 5, "https://static.wikia.nocookie.net/foxhole_gamepedia_en/images/3/3d/Hydra%27s_Whisper_Icon.png/revision/latest/scale-to-width-down/30?cb=20210417155345", 100, 0, 0, 5));
    }

    @After
    public void cleanUp() {
        // Delete all test data
        itemRepository.deleteAll();
    }

    @Test
    public void testCRUD() {
        // Create
        Item newItem = new Item("New item", 7, "", 50, 0, 50, 0);
        itemRepository.save(newItem);
        List<Item> searchItems = itemRepository.findByName(newItem.getName());

        assertEquals(1, searchItems.size());
        assertEquals(newItem.getName(), searchItems.get(0).getName());

        // Update
        newItem.seteMatCost(10);
        itemRepository.save(newItem);
        searchItems = itemRepository.findByName(newItem.getName());

        assertEquals(1, searchItems.size());
        Item updatedItem = searchItems.get(0);
        assertEquals(Integer.valueOf(10), updatedItem.geteMatCost());

        // Delete
        itemRepository.delete(newItem);

        searchItems = itemRepository.findByName(newItem.getName());

        assertEquals(0, searchItems.size());
    }
}
