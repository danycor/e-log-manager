FROM openjdk:17-jdk-alpine

RUN adduser -D elog
USER elog
WORKDIR /

ADD target/manager-0.0.1-SNAPSHOT.jar manager.jar
CMD java -jar manager.jar