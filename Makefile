compile :
    mvn clean package -B -Dspring.datasource.url=jdbc:postgresql://0.0.0.0:5432/elogmanager -Dspring.datasource.username=eloguser -Dspring.datasource.password=postgresqlpassword

run :
    java -Dspring.datasource.url=jdbc:postgresql://0.0.0.0:5432/elogmanager -Dspring.datasource.username=eloguser -Dspring.datasource.password=postgresqlpassword -DAPP_PORT=9000 -jar target/manager-0.0.1.jar

deploy :
    heroku deploy:jar target/manager-0.0.1.jar --app e-log-manager